package cn.lsoft.undoner.model;

/**
 * @author <A HREF="mailto:undoner@gmail.com">undoner</A>
 * @date 04-01-2015
 */
public class RabbitQueue {
    private String name;
    private long messages_ready;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getMessages_ready() {
        return messages_ready;
    }

    public void setMessages_ready(long messages_ready) {
        this.messages_ready = messages_ready;
    }
}

