package cn.lsoft.undoner.service;

import cn.lsoft.undoner.model.MessageRequest;

import java.util.List;

public interface MessageService {
	public boolean saveMessageRequest(MessageRequest request);

	public List<MessageRequest> queryMessageList(MessageRequest request);
}
