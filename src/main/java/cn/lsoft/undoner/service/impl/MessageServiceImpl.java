package cn.lsoft.undoner.service.impl;


import cn.lsoft.undoner.dao.MessageDao;
import cn.lsoft.undoner.model.MessageRequest;
import cn.lsoft.undoner.service.MessageService;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;


@Service
public class MessageServiceImpl implements MessageService {
    private static final Logger log = LoggerFactory.getLogger(MessageServiceImpl.class);

    @Qualifier("messageDaoImpl")
    @Autowired
    private MessageDao messageDao;

    private static final Gson gson = new Gson();

    @Override
    public boolean saveMessageRequest(MessageRequest request) {
        String message = gson.toJson(request);
        log.info("saveMessageRequest: " + message + "\n");
        messageDao.saveMessage(request);
        return true;
    }

    @Override
    public List<MessageRequest> queryMessageList(MessageRequest request) {
        return messageDao.queryMessageList();
    }

}
