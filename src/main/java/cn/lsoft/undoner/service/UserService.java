package cn.lsoft.undoner.service;

import cn.lsoft.undoner.model.MessageRequest;
import cn.lsoft.undoner.model.User;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface UserService {
	public boolean saveUser(User user);

	public List<User> queryUserByUsername(String  userName);

	public boolean queryUserByUser(User user);
}
