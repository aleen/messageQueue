package cn.lsoft.undoner.util;

import com.google.gson.Gson;

public class GsonUtil {
	private GsonUtil() {}
	
	private static final Gson gson = new Gson();
	
	public static Gson getGson() {
		return gson;
	}
}