package cn.lsoft.undoner.exception;

/**
 * @author <A HREF="mailto:undoner@gmail.com">undoner</A>
 * @date 05-06-2014
 */
public class NasException extends Exception {
    public NasException() {
    }

    public NasException(String s) {
        super(s);
    }

    public NasException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public NasException(Throwable throwable) {
        super(throwable);
    }
}
