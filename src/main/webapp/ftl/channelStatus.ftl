<html>
<link media="screen" rel="stylesheet" href="${rc.contextPath}/css/cc.css">
<link rel="stylesheet" type="text/css" href="${rc.contextPath}/css/common.css">
<body>
<div id="header" style="font-family:'arial'">
    <div id="logo" style="font-family:'arial';font-weight:bolder;color:black;float:left;height:30px;left:10px; top:12px;position:absolute; ">
        <font size=5>JobTracker Center</font>
    </div>
</div>
<div id="headerBanner">
    <h3>Welcome,User Name:<span id="user">${Session["login"].userName}</span>
        &nbsp;&nbsp;&nbsp;&nbsp;<a href="${rc.contextPath}/user/">Index</a>
        &nbsp;&nbsp;&nbsp;&nbsp;<a href="${rc.contextPath}/user/quit">Logout</a>
    </h3>
</div>
<div class="border" style="margin-left:10%;margin-right: 10%;margin-top:50px;">
    <h1 align="center" >Capture Info : ${data.captureModelList?size} at server last 1 day</h1>
    <table class="table" style="width: 100%">
        <tr style="background-color: #c8c8c8">
            <th>Capture Id</th>
            <th>Normal Count</th>
            <th>Total Count</th>
        </tr>
    <#list data.captureModelList as capture>
        <tr <#if capture.channelList?size !=capture.normalCount>class="red" </#if> >
            <td>${capture.name}</td>
            <td>${capture.normalCount}</td>
            <td>${capture.channelList?size}</td>
        </tr>
    </#list>
    </table>
</div>
<div class="border" style="margin-left:10%;margin-right: 10%;margin-top:50px;">
    <h1 align="center" >channel Info : ${data.channelModelList?size} at server last 1 day</h1>
    <table class="table" style="width: 100%">
        <tr style="background-color: #c8c8c8">
            <th width="8%">Channel Number</th>
            <th width="20%">Channel Name</th>
            <th width="10%">Display Name</th>
            <th width="25%">Channel Uuid</th>
            <th width="6%">City</th>
            <th width="6%">Backup</th>
            <th width="25%">Capture Url</th>
        </tr>
    <#list data.channelModelList as channel>
        <tr title="
Channel Number:${channel.number},
Channel Status:${channel.status},
Backup:${channel.backup?string},
Station_Id:${channel.stationId},
Station_Number:${channel.stationNumber},
Last Update At:[${channel.captureUrlLastUpdatedAt}]"
            <#if channel.status!="NORMAL">class="red" </#if> >
            <td>${channel.number}</td>
            <td>${channel.name}</td>
            <td>${channel.displayName}</td>
            <td>${channel.uuid}</td>
            <td>${channel.city}</td>
            <td>${channel.backup?string}</td>
            <td>${channel.captureUrl}
                <#if (channel.backup)>
                    <a style="float: right" href="#" onclick="window.open('${channel.captureUrl}/${channel.uuid}/screenshot?isBackup=true');return false;">jpg</a>
                <#else>
                    <a style="float: right" href="#" onclick="window.open('${channel.captureUrl}/${channel.uuid}/screenshot?isBackup=false');return false;">jpg</a>
                </#if>

            </td>

        </tr>
    </#list>
    </table>
</div>
<!--footer-->
<#include "_footer.ftl" />
<!--footer end-->
</body>
</html>