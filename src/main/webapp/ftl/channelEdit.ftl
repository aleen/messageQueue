<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
    <meta http-equiv="expires" content="0">
    <title>JobTracker Center</title>
    <link media="screen" rel="stylesheet" href="${rc.contextPath}/css/cc.css">
    <link rel="stylesheet" type="text/css" href="${rc.contextPath}/css/common.css">
    <script src="${rc.contextPath}/js/jquery.min.js" type="text/javascript"></script>
    <script src="${rc.contextPath}/js/channelEdit.js" type="text/javascript"></script>
</head>
<body>
<!--header-->
<#include "_header.ftl" />
<!--header end-->
<div style="margin-top:80px;margin-left:1%;margin-right: 1%;font-size:14px;font-family:'arial';">
    <p align="center" style="font-size:16px;font-family:'arial';"> Please select company:
        <select id="selectCompany" onchange=updateCompany(this.value) style="width:220px;height:20px;margin:5px;">
        <#list companyList as cm>
            <#if companyId == cm.id>
                <option value=${cm.id} selected="selected">${cm.name}</option>
            <#else>
                <option value=${cm.id}>${cm.name}</option>
            </#if>

            </tr>
        </#list>
        </select>
    </p>
    <br />
    <div id="candidateChannelList" style="width:42%;float:left;box-sizing:border-box;padding-right:10px;">
        <p align="center"><input id="addChannel" onclick="addChannel()" type="button" value="Add Channel" style="height:24px;width:50%;background:RGB(0,169,92);background-size:126px 24px;border:solid 1px #dadee9;line-height:24px;text-align:center;cursor:pointer;color:#fff"></p>
        <br />
        <p style="font-size:16px;font-family:'arial';">Candidate Channels Total:${channelList.candidateChannelSize}
            (<span style="color: red">NO_SIGNAL:Red</span>,<span style="color: green">NORMAL:Green</span>)
        </p>
        <table width="100%" border=1 class="list" id="candidateChannelTableList">
            <tr style="height: 40px;text-align: center">
                <th width="5%"><label><input type="checkbox" class="checkbox" name="recordingAll" onchange="recordingAll()"/></label></th>
                <th width="10%">Number<span class="bv-ui-icon sort asc"></span></th>
                <th width="20%">Channel Name</th>
                <th width="20%">Display Name</th>
                <th width="45%">Channel Uuid</th>
                <th style="display:none;"></th>
            </tr>
        <#list channelList.errCandidateChannelList as errcml>
            <tr  align="center"
                 title="
Channel Number:${errcml.number},
Capture Urls:${errcml.captureChannelHash.channelList?size} ,
Normal Total:${errcml.captureChannelHash.normalCount} ,
Last Update: [${errcml.captureUrlLastUpdatedAt}]
(<#list errcml.captureChannelHash.channelList as errcmlccl>
City:${errcmlccl.city},
Url:${errcmlccl.captureUrl},
Status:${errcmlccl.status},
Backup:${errcmlccl.backup?string},
Station_Id:${errcmlccl.stationId},
Station_Number:${errcmlccl.stationNumber},
Time:${errcmlccl.captureUrlLastUpdatedAt}
-
</#list>)
                 "
                 class="red"
            >
                <td><label><input type="checkbox" class="checkbox" name="recording"/></label></td>
                <td>${errcml.number}</td>
                <td>${errcml.name}</td>
                <td>${errcml.displayName}</td>
                <td>${errcml.uuid}</td>
                <td style="display:none;">${errcml.channelId}</td>
            </tr>
        </#list>
        <#list channelList.normalCandidateChannelList as norcml>
            <tr  align="center"
                 title="
Channel Number:${norcml.number},
Capture Urls:${norcml.captureChannelHash.channelList?size} ,
Normal Total:${norcml.captureChannelHash.normalCount} ,
Last Update: [${norcml.captureUrlLastUpdatedAt}]
(<#list norcml.captureChannelHash.channelList as norcmlccl>
City:${norcmlccl.city},
Url:${norcmlccl.captureUrl},
Status:${norcmlccl.status},
Backup:${norcmlccl.backup?string},
Station_Id:${norcmlccl.stationId},
Station_Number:${norcmlccl.stationNumber},
Time:${norcmlccl.captureUrlLastUpdatedAt}
-
</#list>)
                 "
                 class="green"
                    >
                <td><label><input type="checkbox" class="checkbox" name="recording"/></label></td>
                <td>${norcml.number}</td>
                <td>${norcml.name}</td>
                <td>${norcml.displayName}<#if norcml.status=="NORMAL"><a style="float: right;color: red" href="#" onclick="window.open('${norcml.captureUrl}/${norcml.uuid}/screenshot');return false;">JPG</a></#if></td>
                <td>${norcml.uuid}</td>
                <td style="display:none;">${norcml.channelId}</td>
            </tr>
        </#list>
        <#list channelList.allCandidateChannelList as allchl>
            <tr align="center" >
                <td><label><input type="checkbox" class="checkbox" name="recording"/></label></td>
                <td>${allchl.number}</td>
                <td>${allchl.name}</td>
                <td>${allchl.displayName}</td>
                <td>${allchl.uuid}</td>
                <td style="display:none;">${allchl.channelId}</td>
            </tr>
        </#list>
        </table>
    </div>

    <div id="selectChannelList" style="width:58%;float:left;box-sizing:border-box;padding-left:10px;">
        <p align="center"><input id="removeChannel" onclick="removeChannel()" type="button" value="Remove Channel" style="height:24px;width:25%;background:RGB(0,169,92);background-size:126px 24px;border:solid 1px #dadee9;line-height:24px;text-align:center;cursor:pointer;color:#fff">
            <input id="saveChannel" onclick="saveChannel()" type="button" value="Save Channel" style="height:24px;width:25%;background:RGB(0,169,92);background-size:126px 24px;border:solid 1px #dadee9;line-height:24px;text-align:center;cursor:pointer;color:#fff">
        </p>
        <br />
        <p style="font-size:16px;font-family:'arial';">Selected Channels Total:<span id="SelectedTotal">${channelList.selectChannelSize}</span>
            <span id="VariableShowTotal" style="float: right"></span>
        </p>

        <table width="100%" border=1 id="selectChannelTableList">
            <tr style="height: 40px;text-align: center">
                <th width="5%" ><input type="checkbox" name="selectedAll" onchange="selectedAll()"/></th>
                <th width="8%">Number<span class="bv-ui-icon sort asc"></span></th>
                <th width="21%">Channel Name</th>
                <th width="13%">Display Name</th>
                <th width="32%">Channel Uuid</th>
                <th width="7%">Variable</th>
                <th width="7%">Show</th>
                <th width="7%">Backup</th>
                <th style="display:none;"></th>
            </tr>
        <#list channelList.errSelectedChannelList as errscl>
            <tr align="center"
                title="
Channel Number:${errscl.number},
Capture Urls:${errscl.captureChannelHash.channelList?size} ,
Normal Total:${errscl.captureChannelHash.normalCount} ,
Last Update: [${errscl.captureUrlLastUpdatedAt}]
(<#list errscl.captureChannelHash.channelList as errsclccl>
City:${errsclccl.city},
Url:${errsclccl.captureUrl},
Status:${errsclccl.status},
Backup:${errsclccl.backup?string},
Station_Id:${errsclccl.stationId},
Station_Number:${errsclccl.stationNumber},
Time:${errsclccl.captureUrlLastUpdatedAt}
-
</#list>)
                 "
                class="red"
                    >
                <td><input type='checkbox'  name='selected'></td>
                <td>${errscl.number}</td>
                <td>${errscl.name}</td>
                <td>${errscl.displayName}</td>
                <td>${errscl.uuid}</td>
                <td>
                    <#if errscl.variable>
                        <input type='checkbox' name='variable' checked='checked' onchange="setVariableShowTotal()">
                    <#else>
                        <input type='checkbox' name='variable' onchange="setVariableShowTotal()">
                    </#if>
                </td>
                <td>
                    <#if errscl.show>
                        <input type='checkbox' name='show' checked='checked' onchange="setVariableShowTotal()">
                    <#else>
                        <input type='checkbox' name='show' onchange="setVariableShowTotal()">
                    </#if>
                </td>
                <td>
                    <#if errscl.backup>
                        <input type='checkbox' name='backup' checked='checked' onchange="setVariableShowTotal()">
                    <#else>
                        <input type='checkbox' name='backup' onchange="setVariableShowTotal()">
                    </#if>
                </td>
                <td style="display:none;">${errscl.channelId}</td>
            </tr>
        </#list>
        <#list channelList.normalSelectedChannelList as norscl>
            <tr  align="center"
                 title="
Channel Number:${norscl.number},
Capture Urls:${norscl.captureChannelHash.channelList?size} ,
Normal Total:${norscl.captureChannelHash.normalCount} ,
Last Update: [${norscl.captureUrlLastUpdatedAt}]
(<#list norscl.captureChannelHash.channelList as norsclccl>
City:${norsclccl.city},
Url:${norsclccl.captureUrl},
Status:${norsclccl.status},
Backup:${norsclccl.backup!?string},
Station_Id:${norsclccl.stationId},
Station_Number:${norsclccl.stationNumber},
Time:${norsclccl.captureUrlLastUpdatedAt}
-
</#list>)
                 "
                 class="green"
                    >
                <td><input type='checkbox'  name='selected'></td>
                <td>${norscl.number}</td>
                <td>${norscl.name}</td>
                <td>${norscl.displayName}<#if norscl.status=="NORMAL"><a style="float: right;color: red" href="#" onclick="window.open('${norscl.captureUrl}/${norscl.uuid}/screenshot');return false;">JPG</a></#if></td>
                <td>${norscl.uuid}</td>
                <td>
                    <#if norscl.variable>
                        <input type='checkbox' name='variable' checked='checked' onchange="setVariableShowTotal()">
                    <#else>
                        <input type='checkbox' name='variable' onchange="setVariableShowTotal()">
                    </#if>
                </td>
                <td>
                    <#if norscl.show>
                        <input type='checkbox' name='show' checked='checked' onchange="setVariableShowTotal()">
                    <#else>
                        <input type='checkbox' name='show' onchange="setVariableShowTotal()">
                    </#if>
                </td>
                <td><#if norscl.backup>
                    <input type='checkbox' name='backup' checked='checked' onchange="setVariableShowTotal()">
                <#else>
                    <input type='checkbox' name='backup' onchange="setVariableShowTotal()">
                </#if>
                </td>
                <td style="display:none;">${norscl.channelId}</td>
            </tr>
        </#list>
        <#list channelList.allSelectedChannelList as selectcl>
            <tr align="center" >
                <td><input type='checkbox'  name='selected'></td>
                <td>${selectcl.number}</td>
                <td>${selectcl.name}</td>
                <td>${selectcl.displayName}</td>
                <td>${selectcl.uuid}</td>
                <td>
                    <#if selectcl.variable>
                        <input type='checkbox' name='variable' checked='checked' onchange="setVariableShowTotal()">
                    <#else>
                        <input type='checkbox' name='variable' onchange="setVariableShowTotal()">
                    </#if>
                </td>
                <td>
                    <#if selectcl.show>
                        <input type='checkbox' name='show' checked='checked' onchange="setVariableShowTotal()">
                    <#else>
                        <input type='checkbox' name='show' onchange="setVariableShowTotal()">
                    </#if>
                </td>
                <td><#if selectcl.backup>
                    <input type='checkbox' name='backup' checked='checked' onchange="setVariableShowTotal()">
                <#else>
                    <input type='checkbox' name='backup' onchange="setVariableShowTotal()">
                </#if>
                </td>
                <td style="display:none;">${selectcl.channelId}</td>
            </tr>
        </#list>
        </table>
    </div>
    <div id="contextPath" style="display:none;">${rc.contextPath}</div>
</body>
</html>