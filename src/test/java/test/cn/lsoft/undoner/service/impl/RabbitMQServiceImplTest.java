package test.cn.lsoft.undoner.service.impl;

import cn.lsoft.undoner.model.RabbitQueue;
import cn.lsoft.undoner.service.RabbitMQService;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * @author <A HREF="mailto:undoner@gmail.com">undoner</A>
 * @date 03-11-2014
 */
@ContextConfiguration(locations = "classpath:spring/springMVC.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class RabbitMQServiceImplTest {

    @Autowired
    private RabbitMQService rabbitMQService;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void test(){
        List<RabbitQueue> rabbitQueueList = rabbitMQService.listMessageReadyDeadQueue();
        org.junit.Assert.assertNotNull(rabbitQueueList);
        org.junit.Assert.assertTrue(rabbitQueueList.size() > 0);
    }

    @Test
    public void testReSend() throws Exception {
//        int num = rabbitMQService.reSend("tv_ads_detect_result_queue.dead", "tv_ads_detect_result_queue");
        int num = rabbitMQService.reSend("tv_ads_detect_result_queue.dead", "tv_ads_detect_result_queue", 0);
//        int num = rabbitMQService.reSend("test_to", "test_from");
        Assert.assertTrue(num > 0);
        System.out.println(num);
    }
}
