package test.cn.lsoft.undoner;

import org.junit.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import test.BaseTest;

import java.text.DecimalFormat;

/**
 * @author <A HREF="mailto:undoner@gmail.com">undoner</A>
 * @date 12-17-2014
 */
public class SendTaskTest extends BaseTest {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void testSend() throws Exception {
        String context = "";

        DecimalFormat decimalFormat = new DecimalFormat("00");
        String temp = context.replace("${startTime}", "2014-12-16 16:00:" + decimalFormat.format(11));
        rabbitTemplate.convertAndSend("tv_ads_exchange", "tv_ads_element_queue", temp);
//        for (int i = 0; i < 14; i++) {
//            String temp = context.replace("${startTime}", "2014-12-16 16:00:" + decimalFormat.format(i));
//            rabbitTemplate.convertAndSend("tv_ads_exchange", "tv_ads_element_queue", temp);
//        }
    }
}
